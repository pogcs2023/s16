const firstName = document.querySelector("#first-name");
const lastName = document.querySelector("#last-name");
const questionOne = document.querySelector("#q1");
const answerOne = document.querySelector("#a1");
const questionTwo = document.querySelector("#q2");
const answerTwo = document.querySelector("#a2");
const submitButton = document.querySelector("#submit-button");

console.log(questionOne[questionOne.selectedIndex].value);
console.log(questionTwo[questionTwo.selectedIndex].value);

let correctAnswers = 0;

questionOne.addEventListener("change", function() {

	let questionOneChoice = questionOne[questionOne.selectedIndex].value;

	if (questionOneChoice === "wolf") {
		answerOne.textContent = "Correct!"
		correctAnswers += 1;
	} else {
		answerOne.textContent = "Incorrect, please try again."
	}

})

questionTwo.addEventListener("change", function() {

	let questionTwoChoice = questionTwo[questionTwo.selectedIndex].value;

	if (questionTwoChoice === "boy") {
		answerTwo.textContent = "Correct!"
		correctAnswers = correctAnswers + 1;
	} else {
		answerTwo.textContent = "Incorrect, please try again."
	}

})

submitButton.addEventListener("click", function() {
	alert("Good day, " + firstName.value + " " + lastName.value + ". You have " + correctAnswers + "/2 questions correct.");
})

