/*
[Section] Selecting HTML Elements and Accessing Element Properties
- HTML elements can be selected using the different "document" object methods/functions
- Using the JavaScript HTML DOM, the selected element contains a number of useful properties and even attributes that can be manipulated using JS DOM
- For input fields, a "value" property is accessible to obtain information added by the user
- Methods that select multiple elements returns an HTML collection which can be accessed similar to how array elements are accessed using their index

*/

//const firstName = document.getElementById("first-name");

// The querySelector method is a more flexible method for selecting different elements by their tag name, id and class names

const firstName = document.querySelector("#first-name");

const lastName = document.querySelector("#last-name");
const age = document.querySelector("#age");
const description = document.querySelector("#description");
const subscription = document.querySelector("#subscription");

console.log(firstName);
//we are logging the firstName variable and what it contains

console.log(firstName.value);
//it is still blank

console.log("subscription elements")
//selected with id="subscription"
console.log(subscription);


console.log(subscription.selectedIndex);
console.log(subscription[subscription.selectedIndex]);
console.log(subscription[subscription.selectedIndex].value);

// const firstName = document.getElementsByClassName("first-name");
// console.log(firstName[0].value);

/*
[Section] JavaScript Events
- JavaScript events are responsible for making pages interactive
- Using event listeners, JS can "listen" or watch out for specific events that can be used as triggers for manipulating the DOM
- Syntax
	elementObject.addEventListener("event", function(){
		code to execute
	})
- The "textContent" property defines the "text" displayed on HTML text elements like paragraphs and headers
- The "innerHTML" property defines the HTML content of an HTML element which is useful for adding more HTML elements when needed
*/
const fullNameText = document.querySelector("#full-name-txt");
console.log(fullNameText.textContent);
const ageText = document.querySelector("#age-txt");
const descriptionText = document.querySelector("#description-txt");
const subscriptionText = document.querySelector("#subscription-txt");
const priceText = document.querySelector("#price-txt");

firstName.addEventListener("keyup", function() {
	console.log(firstName.value);
	fullNameText.textContent = "Full Name: " + firstName.value + " " + lastName.value;
})

lastName.addEventListener("keyup", function() {
	console.log(lastName.value);
	console.log("Here's your full name: " + firstName.value + " " + lastName.value);
	fullNameText.textContent = "Full Name: " + firstName.value + " " + lastName.value;
})

age.addEventListener("keyup", function() {
	console.log(age);
	console.log("Here is your age: " + age.value);
	// ageText.textContent = "Age: " + age.value;
	ageText.innerHTML = "Age: Your age is <b>" + age.value + "</b>";
})

description.addEventListener("keyup", function () {
	console.log(description);
	console.log("Here's the description: " + description.value);
	descriptionText.textContent = "Description: " + description.value;
})

subscription.addEventListener("change", function () {

	let subscriptionValue = subscription[subscription.selectedIndex].value;

	subscriptionText.textContent = "Subscription Type: " + subscriptionValue;

	if (subscriptionValue === "basic") {
		priceText.textContent = "Price: 500 PHP";
		console.log("Hi, " + firstName.value + " " + "! Basic is index " + subscription.selectedIndex);
	} else {
		priceText.textContent = "Price: 1000 PHP";
		console.log(`Hi ${firstName.value}! Premium is index ${subscription.selectedIndex}!`);
	}

})

